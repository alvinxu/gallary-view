https://alvinxu.gitlab.io/gallary-view/

setKeyEvents() //setup key events
    |-- buttonSearch Click --> fetchData()
    |-- Enter on inputCity --> buttonSearch Click
    |-- ArrowLeft --> prevPage()
    |-- ArrowRight --> nextPage()
    |-- Enter on pageDestNumberInput --> cbJumpPage()
    |-- switch to next picture on mouse-click/ Enter-key/Space-key --> changeImg()
    |-- contextmenu event --> cbContextMenu(e)
        |-- Click to Open
        |-- Click to Copy to Clipboard in Regular size
            |-- convertToPNG()
        |-- Click to save

fetchData()
    |-- renderImages()
        |-- createCarousel() //if not Null clean it
            |-- resetData()
            |-- renderBodyBackground()
                |-- renderBackground()
        |-- renderCarousel()
            |-- renderBackground()
    |-- showPageNumber()

//following are dynamic event listeners
objs.imgContainers.addEventListeners
    |-- click -->
        |-- cbImgClick(e)
            |-- renderBodyBackground
        |-- setImageBorder(e)
    |-- mouseenter --> cbImgMouseenter
        |-- renderBackground()
    |-- mouseLeave --> cbImgMouseleave
        |-- renderBackground()

Bugs：
跳转越界
    //需要把input框里截取下来的string转换成number：
        parseInt()
新搜索清零cursor
    //fetchData()中的cursor:
        初始化 --> no reset
        新搜索 --> reset
        跳转、翻页 --> no reset
当把background-size加了！important以后才能正常显示，不知道哪里有冲突？

需要发生渲染背景和图片的具体动作 --> 统一用renderBackground()实现
101,renderCarousel(), 1 usage
106,renderBodyBackground(), 2 usages
123,mouseenter event
127,mouseleave event

//Deploy pages on Gitlab:

1. install gitlab runner on Ubuntu in VirtualBox
2. register gitlab-runner
3. start gitlab-runner
4. install docker:  sudo apt install docker.io
5. push html/js/css to main branch at Gitlab
6. edit the .yml file to start CI/CD pipeline
