//get all the DOM elements used for rending
let data = {
    unsplashKey: 'xQ0xs70zTCU2NpHv_6CiowMAGCaCfe2eLLzb9C2REq0',
    searchedTimes: 0,
    imgUrlFull: [],
    imgUrlRegular: [],
    imgUrlThumb: [],
    prevSearchKey: null,
    currentUrl: null,
    currentIndex: 0
}

let objs = {
    body: document.querySelector('body'),
    bodyDiv: document.querySelector('.overAll'),
    inputCity: document.querySelector('.searchBar .inputCity'),
    buttonSearch: document.querySelector('.searchBar .btnSearch'),
    carousel: document.querySelector('.carousel'),
    prevBtn: document.querySelector('.prev'),
    nextBtn: document.querySelector('.next'),
    pageNumSpan: document.querySelector('.pageNumber'),
    pageDestNumInput: document.querySelector('.pageDestNumber'),
    menu: document.querySelector('.menuContainer'),
    openMenu: document.querySelector('#openMenu'),
    copyMenu: document.querySelector('#copyMenu'),
    saveMenu: document.querySelector('#saveMenu'),
    jumpButton: document.querySelector('.pageNav button'),
    imgContainers: null,
    page: {
        cursor: 1,
        total: 1
    }
}

//fetch the pictures from backend
const fetchData = () => {
    const newCity = objs.inputCity.value.trim().toLowerCase() || 'Chicago'
    if (newCity !== data.prevSearchKey && data.prevSearchKey) objs.page.cursor = 1
    data.prevSearchKey = newCity
    fetch(`https://api.unsplash.com/search/photos?client_id=${data.unsplashKey}&query=${newCity}&orientation=landscape&page=${objs.page.cursor}`)
        .then(response => response.json())
        .then(d => {
            // console.log('data fetched:', data)
            objs.page.total = d.total_pages
            showPageNumber()
            renderImages(d.results)
            data.searchedTimes++
            showAndHide(data.searchedTimes)
            if (data.searchedTimes === 1) setKeyEvents()
        })
}

const showPageNumber = () => {
    objs.pageNumSpan.innerHTML = `Current page:<span style="font-size:20px;
                                    color:orange;font-weight:bold"> 
                                    ${objs.page.cursor}</span>/${objs.page.total}`
}

//rendering the screen
const renderImages = arrImages => {
    if (arrImages.length !== 0) {

        //render the body background with the fetched image
        let imgUrl = arrImages[0].urls.full
        renderBodyBackground(imgUrl)

        //create the carousel
        createCarousel(arrImages)
        renderCarousel(objs.imgContainers)
    } else {
        alert("No data fetched from back-end")
    }
}

const resetData = () => {
    // objs.imgContainers.forEach(img => objs.carousel.removeChild(img))
    objs.carousel.innerHTML = ''
    objs.imgContainers = null
    data.imgUrlThumb = []
    data.imgUrlRegular = []
    data.imgUrlFull = []
    data.currentIndex = 0
    data.currentUrl = ''
}

const createCarousel = arrImages => {
    //reset data array
    if (objs.imgContainers) resetData()

    //create carousel
    for (let i = 0; i < arrImages.length; i++) {
        let carouselItem = document.createElement('div')
        carouselItem.className = 'imgContainer'
        carouselItem.dataset.index = i
        carouselItem.style.animation = 'fadeIn 0.3s'
        carouselItem.style.animationDelay = `${0.1 * i}s`
        objs.carousel.appendChild(carouselItem)
        data.imgUrlFull.push(arrImages[i].urls.full)
        data.imgUrlRegular.push(arrImages[i].urls.regular)
        data.imgUrlThumb.push(arrImages[i].urls.thumb)
    }

    data.currentIndex = 0
    objs.imgContainers = document.querySelectorAll('.imgContainer')
    objs.imgContainers[data.currentIndex].classList.remove('selected')
    objs.imgContainers[data.currentIndex].classList.add('selected')

    //put some imgContainer events (which are dynamic)
    objs.imgContainers.forEach(img => img.addEventListener('click', e => {
        cbImgClick(e)
        setImageBorder(e)
    }))
    objs.imgContainers.forEach(img => img.addEventListener('mouseenter', cbImgMouseenter))
    objs.imgContainers.forEach(img => img.addEventListener('mouseleave', cbImgMouseleave))
}

const renderCarousel = imgContainers => {
    imgContainers.forEach(imgContainer => {
        let i = imgContainer.dataset.index
        renderBackground(imgContainer, data.imgUrlThumb[i])
        imgContainer.style.backgroundSize = 'contain'
    })
}

const renderBodyBackground = (imgUrl) => {
    renderBackground(objs.bodyDiv, imgUrl)
    data.currentUrl = imgUrl
}

const renderBackground = (obj, url) => {
    obj.style.backgroundImage = `url('${url}')`
    obj.style.backgroundPosition = 'center center'
    obj.style.backgroundAttachment = 'fixed'
    obj.style.backgroundRepeat = 'no-repeat'
}
const cbImgClick = e => {
    let imgUrl = data.imgUrlFull[e.target.dataset.index]
    renderBodyBackground(imgUrl)
}

const setImageBorder = e => {
    objs.imgContainers[data.currentIndex].classList.remove('selected')
    objs.imgContainers[e.target.dataset.index].classList.add('selected')
    data.currentIndex = e.target.dataset.index
}

const cbImgMouseenter = e => {
    let imgUrl = data.imgUrlFull[e.target.dataset.index]
    renderBackground(objs.bodyDiv, imgUrl)
}
const cbImgMouseleave = e => {
    let imgUrl = data.currentUrl
    renderBackground(objs.bodyDiv, imgUrl)
}

//operation on prev button
const prevPage = () => {
    if (objs.page.cursor <= 1) {
        alert('Oops, This is the first page...')
    } else {
        objs.page.cursor--
        fetchData()
    }
}

//operation on next button
const nextPage = () => {
    if (objs.page.cursor >= objs.page.total) {
        alert('Oops, This is the last page...')
    } else {
        objs.page.cursor++
        fetchData()
    }
}

const cbJumpPage = () => {
    const pageNum = parseInt(objs.pageDestNumInput.value.trim().toLowerCase())
    console.log(isNaN(pageNum))
    if (isNaN(pageNum)) alert('No valid input!')
    else if (pageNum < 1 || pageNum > objs.page.total) alert('Invalid page number!')
    else {
        objs.page.cursor = pageNum
        fetchData()
    }

}

const changeImg = () => {

    //disappear the contextmenu if any
    objs.menu.classList.remove('show')

    if (data.currentIndex >= data.imgUrlFull.length - 1) {
        nextPage()
    } else {
        objs.imgContainers[data.currentIndex].classList.remove('selected')
        data.currentIndex++
        let imgUrl = data.imgUrlFull[data.currentIndex]
        renderBodyBackground(imgUrl)
        objs.imgContainers[data.currentIndex].classList.add('selected')
    }
}

const cbContextMenu = e => {
    e.preventDefault()
    if (e.target === objs.bodyDiv) {
        objs.menu.style.left = `${e.pageX}px`
        objs.menu.style.top = `${e.pageY}px`
        objs.menu.classList.add('show')
    }
}

const convertToPNG = blob =>
    new Promise(resolve => {
        const img = new Image();
        img.src = URL.createObjectURL(blob);
        img.onload = () => {
            const canvas = Object.assign(document.createElement('canvas'), {
                width: img.width,
                height: img.height
            });
            canvas.getContext('2d').drawImage(img, 0, 0);
            canvas.toBlob(resolve, 'image/png');
            URL.revokeObjectURL(img.src)
        };
    });

const setKeyEvents = () => {

    //click event on each image in carousel(perform when fetchData() because it's dynamic)
    //mouseenter event on each image in carousel(perform when fetchData() because it's dynamic)
    //mouseleave event on each image in carousel(perform when fetchData() because it's dynamic)

    //prev and next buttons click events
    objs.prevBtn.addEventListener('click', e => prevPage())
    objs.nextBtn.addEventListener('click', e => nextPage())

    //ArrowLeft and ArrowRight for pages
    objs.body.addEventListener('keyup', e => {
        if (e.key === "ArrowLeft") {
            prevPage()
        }
    })
    objs.body.addEventListener('keyup', e => {
        if (e.key === "ArrowRight") {
            nextPage()
        }
    })

    //jump page
    objs.pageDestNumInput.addEventListener('keyup', e => {
        if (e.key === 'Enter') objs.jumpButton.click()
    })
    objs.jumpButton.addEventListener('click', cbJumpPage)

    //switch to next picture on mouse-click/ Enter-key/Space-key
    objs.bodyDiv.addEventListener('click', e => {
        if (e.target === objs.bodyDiv) changeImg()
    })
    objs.body.addEventListener('keyup', e => {
        if (e.key === " " || e.key === "Enter") changeImg()
    })

    //context menu
    objs.bodyDiv.addEventListener('contextmenu', cbContextMenu)

    //Open menu listener
    objs.openMenu.addEventListener('click', () => {
        let url = data.imgUrlFull[data.currentIndex]
        window.open(url)
    })

    //Copy menu listener
    objs.copyMenu.addEventListener('click', () => {
        fetch(`${data.imgUrlRegular[data.currentIndex]}`)
            .then(response => response.blob())
            .then(blob => convertToPNG(blob))
            .then(pngBlob => {
                const item = new ClipboardItem({'image/png': pngBlob})
                navigator.clipboard.write([item])
                    .then(() => alert('Succeed! copied to clipboard!'), error => alert(error))
            })
    })

    //Save as file listener
    objs.saveMenu.addEventListener('click', () => {
        fetch(`${data.imgUrlFull[data.currentIndex]}`)
            .then(response => response.blob())
            .then(blob => {
                let link = document.createElement('a')
                link.href = URL.createObjectURL(blob);
                link.download = 'image.jpg'
                link.click();
                URL.revokeObjectURL(link.href)

            })
    })

}

const showAndHide = (searchedTimes) => {
    if (searchedTimes === 0) {
        objs.inputCity.classList.add('show')
        objs.buttonSearch.classList.add('show')
        objs.carousel.classList.add('hide')
        objs.prevBtn.classList.add('hide')
        objs.nextBtn.classList.add('hide')
        objs.pageDestNumInput.classList.add('hide')
        objs.jumpButton.classList.add('hide')
    }
    if (searchedTimes === 1) {
        objs.inputCity.classList.remove('show')
        objs.buttonSearch.classList.remove('show')
        objs.carousel.classList.remove('hide')
        objs.prevBtn.classList.remove('hide')
        objs.nextBtn.classList.remove('hide')
        objs.pageDestNumInput.classList.remove('hide')
        objs.jumpButton.classList.remove('hide')
    }
}

//initialization
objs.bodyDiv.style.backgroundColor = "rgba(0,0,0,0.9)"
showAndHide(data.searchedTimes)

objs.buttonSearch.addEventListener('click', () => fetchData())

objs.inputCity.addEventListener('keyup', (e) => {
    if (e.key === 'Enter') objs.buttonSearch.click()
})